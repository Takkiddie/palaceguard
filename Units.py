from virtualUnits import *
		
#Non virtual Units
class Enemy(AiUnit):
	def __init__(self,position=1):
		self.setName(position)
		self.picName = 'sprite'
		self.defaultDirectory = 'enemy'
		self.attackPrepDirectory1 = self.defaultDirectory+'Attack1'
		self.attackPrepDirectory2 = self.defaultDirectory+'Attack2'
		#Direction object faces when starting.
		self.direction = 'D'
		#Location of the object's core relative to the picture
		self.xOffset = -150
		self.yOffset = -150
		#As close as another object can be and "touch" this one.
		self.xMinDistance = 100;
		self.yMinDistance = 100;
		#X and Y starting location for object
		self.xStart = self.xMinDistance*position*2
		self.yStart = 100
		#Stats
		self.attackWindup = .5
		self.attackWinddown = .5
		self.attackPower = 5;
		self.hitPoints = 25;
		super().__init__()
	def setName(self,position):
		self.name = 'Enemy'+str(position)
	def deathAnimation(self):
		self.directory = self.defaultDirectory+'Dead';
		#do this in steps.
		#Get time of death and do the animation.
		fullPicName = 'sprite.png'
		return self.grabGrapic(fullPicName)
		
#Non virtual Units
class Ally(AiUnit):
	def __init__(self,position=1):
		self.setName(position)
		self.picName = 'sprite'
		self.defaultDirectory = 'ally'
		self.attackPrepDirectory1 = self.defaultDirectory+'Attack1'
		self.attackPrepDirectory2 = self.defaultDirectory+'Attack2'
		#Direction object faces when starting.
		self.direction = 'D'
		#Location of the object's core relative to the picture
		self.xOffset = -150
		self.yOffset = -150
		#As close as another object can be and "touch" this one.
		self.xMinDistance = 100;
		self.yMinDistance = 100;
		#X and Y starting location for object
		self.xStart = self.xMinDistance*position*2
		self.yStart = 300
		#Stats
		self.attackWindup = .5
		self.attackWinddown = .5
		self.attackPower = 5;
		self.hitPoints = 25;
		super().__init__()
	def setName(self,position):
		self.name = 'Ally'+str(position)
	def deathAnimation(self):
		self.directory = self.defaultDirectory+'Dead';
		#do this in steps.
		fullPicName = 'sprite.png'
		return self.grabGrapic(fullPicName)

#Teumessian is the Hero's name
class Hero(AttackingUnit):
	def __init__(self):
		self.picName = 'sprite'
		self.defaultDirectory = 'hero'
		self.attackPrepDirectory1 = "heroAttack1"
		self.attackPrepDirectory2 = "heroAttack2"
		#Direction object faces when starting.
		self.direction = 'D'
		#Location of the object's core relative to the picture
		self.xOffset = -150
		self.yOffset = -150
		#X and Y starting location for object
		self.xStart = 100
		self.yStart = 100
		#As close as another object can be and "touch" this one.
		self.xMinDistance = 100
		self.yMinDistance = 100
		self.attackWindup = .5
		self.attackWinddown = .2
		self.attackPower = 5
		self.hitPoints = 5
		self.killCount = 20
		self.WhackPrepped = False
		super().__init__()
	def prepWhack(self):
		#change attack directories as well.
		self.defaultDirectory = 'whackHero'
		self.attackPrepDirectory1 = "whackHeroAttack1"
		self.attackPrepDirectory2 = "whackHeroAttack2"
		self.WhackPrepped = True;
		return
	def executeAttack(self,attackPower):
		if(self.WhackPrepped):
			attackPower += self.killCount
		super().executeAttack(attackPower)
	def finishAttack(self):
		self.WhackPrepped = False;
		self.defaultDirectory = 'hero'
		self.attackPrepDirectory1 = "heroAttack1"
		self.attackPrepDirectory2 = "heroAttack2"
		super().finishAttack()	
	def killedTarget(self):
		if(self.WhackPrepped):
			self.killCount += 1
		super().killedTarget()

		
#Static Units
class WhackButton(Unit):
	def __init__(self):
		#Direction object faces when starting.
		#Location of the object's core relative to the picture
		self.xOffset = 0
		self.yOffset = 0
		#X and Y starting location for object
		self.xStart = 0
		self.yStart = 0
		self.directory = '.'
		fullPicName = "whackButton.png";
		self.pic = self.grabGrapic(fullPicName);
		self.picpos = self.pic.get_rect()
		super().__init__()
