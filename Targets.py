class Target:
	def x(self):
		return self.xPos
	def y(self):
		return self.yPos
	def minY(self):
		return self.yMinDistance
	def minX(self):
		return self.xMinDistance
	def hasBody(self):
		return (self.minX() + self.minX()) > 0
	def targetEnabled(self):
		return True;
		
class PointTarget(Target):
	def __init__(self,x,y):
		self.xMinDistance = 0
		self.yMinDistance = 0
		self.xPos = x
		self.yPos = y

class HitableTarget(Target):
	def getHit(self, EnemyAttack):
		self.hitPoints = self.hitPoints - EnemyAttack;
		if(self.isDead()):
			self.xMinDistance = 0
			self.yMinDistance = 0
			self.die();
	def isDead(self):
		return (self.hitPoints <= 0)
	def targetEnabled(self):
		return not self.isDead();
