from SuperScreen import *
from Units import *
import time

#Goal today: multiple enemies in array.

class BattleScreen(SuperScreen):
	def __init__(self,screen,winDisp):
		self.superInit(screen,winDisp)
		self.enemies = [];
		self.enemies.append(Enemy(1))
		self.enemies.append(Enemy(3))

		self.allies = [];
		self.allies.append(Ally(1))
		self.allies.append(Ally(3))

		self.enemyDisps = {}
		self.hero = Hero()
		self.whackButton = WhackButton()
	def collide(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				self.winDisp.running = False;
			if event.type == pygame.MOUSEBUTTONDOWN : 
				x, y = event.pos
				enemy = self.clickedOnEnemy(x,y)
				if(enemy):
					self.hero.setTarget(enemy)
				elif(self.clickedOnWhack(x,y)):
					self.hero.prepWhack()
				else:
					self.hero.setTarget(PointTarget(x,y))
	def displayScreen(self):
		self.screen.fill(self.white)
		self.hero.moveUnit()
		self.displayEnimies()
		self.displayAllies()
		self.assignTargets()
		#TODO Figure out a way to display the 'higher' units first.
		self.whackDisp = self.displayUnit(self.whackButton)
		self.heroDisp = self.displayUnit(self.hero)
	def displayEnimies(self):
		self.enemyDisps = {}
		keys = enumerate(self.enemies)
		for index,value in keys:
			self.enemies[index].moveUnit()
			self.enemyDisps[index] = self.displayUnit(self.enemies[index])
	def displayAllies(self):
		keys = enumerate(self.allies)
		for index,value in keys:
			self.allies[index].moveUnit()
			self.displayUnit(self.allies[index])
	def displayUnit(self,unit):
		pic = unit.getPic()
		ret = self.screen.blit(pic, unit.picpos)
		return ret
	def clickedOnWhack(self,x,y):
		if(self.whackDisp.collidepoint(x,y)):
			return True;
		return False;
	def assignTargets(self):
		enemyKeys = enumerate(self.enemies)
		allyKeys = enumerate(self.allies)
		for index,value in enemyKeys:
			self.enemies[index].assignTargetFrom(self.allies)
		for index,value in allyKeys:
			self.allies[index].assignTargetFrom(self.enemies)
	def clickedOnEnemy(self,x,y):
		keys = enumerate(self.enemies)
		for index,value in keys:
			if(self.enemyDisps[index].collidepoint(x,y) and self.enemies[index].targetEnabled()):
				return self.enemies[index]
		return False;
		