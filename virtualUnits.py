from SuperScreen import *
from Targets import *
from functools import wraps
import time

saveOutputDict = {}
def saveOutput(function):
	@wraps(function)
	def wrapped(*args, **kwargs):
		inputKey = str([args, kwargs, function])
		if(inputKey in saveOutputDict):
			ret = (saveOutputDict[inputKey])
		else:
			ret = function(*args, **kwargs)
			saveOutputDict[inputKey] = ret
		return ret
	return wrapped

@saveOutput
def loadify(img):
	ret = pygame.image.load(img).convert_alpha()
	return ret

class Unit(HitableTarget):
	def __init__(self):
		self.picpos.x = self.xStart
		self.picpos.y = self.yStart
		self.standStill()
	def getPic(self):
		return self.pic
	def contactingTarget(self):
		#todo throw error if this is ever called as-is.
		#This is meant to be a virtual method.
		return;
	def moveUnit(self):
		if(self.closeEnough()):
			self.contactingTarget()
			return False
		moveX = self.getMove(self.x(),self.target.x());
		moveY = self.getMove(self.y(),self.target.y());
		self.picpos = self.picpos.move(moveX,moveY);
		return True;
	def getMove(self,position,destination):
		if(position < destination):
			return 1;
		elif(position > destination):
			return -1;
		else:
			return 0;
	def closeEnough(self):
		if(not self.target.hasBody()):
			return False
		minX = self.minX() + self.target.minX()
		minY = self.minY() + self.target.minY()
		xClose = (abs(self.x() - self.target.x()) <= minX)
		yClose = (abs(self.y() - self.target.y()) <= minY)
		return (xClose and yClose)
	def setTarget(self,target):
		self.target = target;
	def x(self):
		return self.picpos.x - self.xOffset
	def y(self):
		return self.picpos.y - self.yOffset
	def grabGrapic(self,file):
		ret = None;
		#Maybe add a unit-level directory to keep things straight. It's already pretty messy.
		imagePath = os.path.join('assets', 'graphics',self.directory,file);	
		ret = loadify(imagePath);
		return ret;
	def standStill(self):
		initX = self.getX()
		initY = self.getY()
		self.target = PointTarget(initX,initY)
	def getX(self):
		return self.xOffset*-1+self.picpos.x
	def getY(self):
		return self.yOffset*-1+self.picpos.y
	def returnLarger(self,x,y):
		if(x > y):
			return x
		else:
			return y
	def isCloserToCurrent(self,current,challenger):
		currentDistanceX = abs(self.getX() - current.getX())
		currentDistanceY = abs(self.getY() - current.getY())
		challengerDistanceX = abs(self.getX() - challenger.getX())
		challengerDistanceY = abs(self.getY() - challenger.getY())
		
		currentDistance = self.returnLarger(currentDistanceX, currentDistanceY)
		challengerDistance = self.returnLarger(challengerDistanceX, challengerDistanceY)

		#If either is dead, select the other. 
		if(current.isDead()):
			challengerDistance = 0
		elif(challenger.isDead()):
			currentDistance = 0
		
		return currentDistance < challengerDistance
	def getName(self):
		return self.name
		
class DirectionalUnit(Unit):
	def __init__(self):
		super().__init__();
	def getPic(self):
		if(self.isDead()):
			return self.deathAnimation()
		fullPicName = self.picName + self.direction + '.png'
		pic = self.grabGrapic(fullPicName);
		return pic
	def moveUnit(self):
		if(self.isDead()):
			return False
		newDirection = '';
		
		if(self.picpos.x - self.xOffset < self.target.x()):
			newDirection = newDirection+'R';
		elif(self.picpos.x - self.xOffset > self.target.x()):
			newDirection = newDirection+'L';
		if(self.picpos.y - self.yOffset < self.target.y()):
			newDirection = newDirection+'D';
		elif(self.picpos.y - self.yOffset > self.target.y()):
			newDirection = newDirection+'U';

		if(newDirection != ''):
			self.direction = newDirection;			
		elif(self.direction == ''):
			self.direction = self.direction+'D';
		return super().moveUnit()

class AttackingUnit(DirectionalUnit):
	def __init__(self):
		self.attackInitiated = False
		self.lockedMovement = False
		self.attackExecuted = False
		self.attackBegan = 0
		self.directory = self.defaultDirectory
		self.pic = self.getPic()
		self.picpos = self.pic.get_rect()
		super().__init__()
	def contactingTarget(self):
		self.InitiateAttack();
	def InitiateAttack(self):
		#Start Attack
		if(not self.attackInitiated):
			self.attackInitiated = True
			self.directory = self.attackPrepDirectory1;
			self.attackBegan = time.time()
		#Process Hit, and lock movement
		elif(not self.attackExecuted and time.time() > (self.attackBegan+self.attackWindup)):
			self.executeAttack(self.attackPower)
		#finalize attack (Will execute last)
		elif(time.time() > (self.attackBegan+self.attackWindup+self.attackWinddown)):
			self.finishAttack()
	def moveUnit(self):
		if(self.isDead()):
			return super().moveUnit()
		if(not self.lockedMovement):
			if(super().moveUnit()):
				self.directory = self.defaultDirectory
				self.attackInitiated = False
				return True;
		else:
			self.contactingTarget();
		return False;
	def executeAttack(self,attackPower):
		self.target.getHit(attackPower);
		self.directory = self.attackPrepDirectory2;
		self.lockedMovement = True
		self.attackExecuted = True
	def finishAttack(self):
		self.attackInitiated = False
		self.lockedMovement = False
		self.attackExecuted = False
		self.attackInitiated = False
		return
	def die(self):
		self.timeOfDeath = time.time()
		self.attackInitiated = False
		self.lockedMovement = False
		self.attackExecuted = False
		self.attackInitiated = False
		self.standStill()
	def killedTarget(self):
		self.target.die()
		self.standStill()
	def hasValidTarget(self):
		return self.target.hasBody() and self.target.targetEnabled()
	def assignTargetFrom(self, list):
		if self.hasValidTarget() or not list:
			return;
		keys = enumerate(list)
		current_target = None
		for index,value in keys:
			if(current_target == None):
				current_target = index;				
			if(not self.isCloserToCurrent(list[current_target],list[index])):
				current_target = index;
		if(current_target != None):
			self.target = list[current_target]
class AiUnit(AttackingUnit):
	def __init__(self):
		super().__init__()
	def moveUnit(self):
		if(self.hasValidTarget()):
			return super().moveUnit()
		else:
			self.attackInitiated = False
			self.lockedMovement = False
			self.attackExecuted = False
			self.attackInitiated = False
			self.standStill()
			return super().moveUnit()
			

		
