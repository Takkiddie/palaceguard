import sys, pygame, os
import time


#Has all of the information about the window
class SuperScreen:
	def superInit(self,screen,winDisp):
		self.winDisp = winDisp
		self.size = width, height = 1000, 660
		self.white = 255, 255, 255
		self.screen = screen
	def displayScreen(self):
		self.screen.fill(self.white)
	def collide(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				self.winDisp.running = False;
			if event.type == pygame.MOUSEBUTTONDOWN : 
				x, y = event.pos
				print(x,y)
	def updateScreen(self):
		start = time.time()	
		frameLength = .002;

		#Usually takes ~.001 to execute
		#Often takes ~0.0 to execute
		self.displayScreen()
		pygame.display.flip()
		self.collide()
		
		#Do not render another frame until frameLength has passed.
		while (start+frameLength > time.time()):
			#TODO: This is inefficient. Have it do the math and wait instead. 
			continue;

		return self.winDisp.running

		