class Display:
	#This is a static class.
	_instance = None
	running = True;
	def __new__(cls, *args, **kwargs):
		if not cls._instance:
			cls._instance = super(Display, cls).__new__(cls, *args, **kwargs)
			
		return cls._instance